package class

import (
	"github.com/micmonay/keybd_event"
)

func press(key string) {
	switch key {
	case "pageup":
		kb.SetKeys(keybd_event.VK_PAGEUP)
	case "pagedown":
		kb.SetKeys(keybd_event.VK_PAGEDOWN)
	case "f5":
		kb.SetKeys(keybd_event.VK_F5)
	default:
		println("Failed to operate keyboard.")
		return
	}
	kb.Launching()
}
