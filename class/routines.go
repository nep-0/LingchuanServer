package class

import (
	"fmt"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var mu = sync.Mutex{}

func (c *class) sendRoutine(conn *websocket.Conn) {
	client := c.Students[conn]
	var msg string
	for {
		msg = <-client.send
		if msg == "/stop" {
			c.Students[conn].handleWaitGroup.Done()
			return
		}
		conn.WriteMessage(1, []byte(msg))
	}
}

func (c *class) receiveRoutine(conn *websocket.Conn) {
	defer func() {
		conn.Close()
		fmt.Println("Connection with", c.Students[conn].name, "closed.")
		c.Students[conn].receive <- "/stop"
		c.Students[conn].send <- "/stop"
		c.Students[conn].handleWaitGroup.Done()
	}()
	client := c.Students[conn]
	var msg []byte
	var err error
	for {
		conn.SetReadDeadline(time.Now().Add(time.Second * time.Duration(120)))
		_, msg, err = conn.ReadMessage()
		if err == nil {
			if msg != nil {
				if string(msg) == "/htbt" || string(msg) == "/stop" {
					// Do not process.
				} else {
					client.receive <- string(msg)
				}
				continue
			}
		}
		break
	}
}

func (c *class) handleMessage(conn *websocket.Conn) {
	var msg string
	for {
		msg = <-c.Students[conn].receive
		if msg == "/stop" {
			c.Students[conn].handleWaitGroup.Done()
			return
		}

		switch c.inter.form {
		case "disc":
			fmt.Println(c.Students[conn].name, ":", msg)
			c.Boardcast(c.Students[conn].name + " : " + msg)
			if c.teacher.connected {
				c.teacher.send <- c.Students[conn].name + " : " + msg
			}
		case "abcd":
			switch msg {
			case "A":
				c.inter.collect[conn] = "A"
			case "B":
				c.inter.collect[conn] = "B"
			case "C":
				c.inter.collect[conn] = "C"
			case "D":
				c.inter.collect[conn] = "D"
			default:
				c.Students[conn].send <- "INVALID ANSWER!"
			}
		case "fill":
			c.inter.collect[conn] = msg
		case "race":
			mu.Lock()
			if c.inter.form == "race" {
				c.inter.collect[conn] = msg
				c.EndInteraction()
			}
			mu.Unlock()
		}
	}
}
