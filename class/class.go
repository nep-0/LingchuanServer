package class

import (
	"LingchuanServer/server"
	"fmt"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var runningWaitGroup sync.WaitGroup

type student struct {
	name            string
	send            chan string
	receive         chan string
	handleWaitGroup sync.WaitGroup
}

func NewStudent(name string) *student {
	return &student{
		name:            name,
		send:            make(chan string, 16),
		receive:         make(chan string, 16),
		handleWaitGroup: sync.WaitGroup{},
	}
}

func (c *class) classHandle(conn *websocket.Conn) {
	var name string

	conn.SetReadDeadline(time.Now().Add(time.Millisecond * time.Duration(10000)))
	_, msg, err := conn.ReadMessage()
	if err != nil {
		conn.Close()
		fmt.Println("Connection with", conn.RemoteAddr(), "closed.")
		return
	}
	name = string(msg)

	// time out
	if name == "" {
		conn.Close()
		fmt.Println("Connection with", conn.RemoteAddr(), "closed.")
		return
	}

	conn.SetReadDeadline(time.Now().Add(time.Millisecond * time.Duration(10000)))
	_, msg, err = conn.ReadMessage()
	if err != nil {
		conn.Close()
		fmt.Println("Connection with", conn.RemoteAddr(), "closed.")
		return
	}
	password := string(msg)

	if !c.auth(name, password) {
		conn.Close()
		fmt.Println("Connection with", conn.RemoteAddr(), "closed.")
		return
	}

	c.Students[conn] = NewStudent(name)
	c.Students[conn].send <- "Your name is " + name
	fmt.Println(conn.RemoteAddr(), "is", name)

	// tell students who attended during the class
	switch c.inter.form {
	case "disc":
		c.Students[conn].send <- "/disc"
	case "abcd":
		c.Students[conn].send <- "/abcd"
	case "fill":
		c.Students[conn].send <- "/fill"
	case "race":
		c.Students[conn].send <- "/race"
	}

	c.Students[conn].handleWaitGroup.Add(3)
	go c.receiveRoutine(conn)
	go c.sendRoutine(conn)
	go c.handleMessage(conn)
	c.Students[conn].handleWaitGroup.Wait()

	delete(c.Students, conn)
}

type class struct {
	server   *server.WsServer
	Students map[*websocket.Conn]*student
	teacher  *teacher
	auth     func(string, string) bool
	inter    *interaction
}

func NewClass() *class {
	ret := &class{
		// server:   server.NewWsServer("0.0.0.0:1234", classHandle),
		Students: make(map[*websocket.Conn]*student, 100),
		inter:    &discuss,
		auth: func(string, string) bool {
			// default authorization function
			return true
		},
		teacher: &teacher{
			auth: func(string, string) bool {
				return true
			},
			send:      make(chan string, 16),
			receive:   make(chan string, 16),
			connected: false,
		},
	}
	ret.server = server.NewWsServer("0.0.0.0:1234", ret.classHandle)
	ret.teacher.server = server.NewWsServer("0.0.0.0:2345", ret.teacher.teacherHandle)
	return ret
}

func (c *class) Start() {
	go c.teacher.server.Start()
	go c.server.Start()
	runningWaitGroup.Add(1)
	// go c.TeacherCmd()
	go c.teacherMessage()
	runningWaitGroup.Wait()
}

/*
func (c *class) TeacherCmd() {
	var input = bufio.NewReader(os.Stdin)
	var inputString []byte
	for {
		input = bufio.NewReader(os.Stdin)
		inputString, _ = input.ReadSlice(byte('\n'))
		switch string(inputString) {
		case "abcd\n":
			input = bufio.NewReader(os.Stdin)
			inputString, _ = input.ReadSlice(byte('\n'))
			c.Abcd(string(inputString[:len(inputString)-1]))
		case "fill\n":
			input = bufio.NewReader(os.Stdin)
			inputString, _ = input.ReadSlice(byte('\n'))
			c.Fill(string(inputString[:len(inputString)-1]))
		case "race\n":
			c.Race()
		case "endi\n":
			c.EndInteraction()
		case "exit\n":
			runningWaitGroup.Done()
			return
		default:
			c.Boardcast(string(inputString[:len(inputString)-1]))
		}
	}
}
*/
