package server

import (
	"fmt"
	"net"
	"net/http"

	"github.com/gorilla/websocket"
)

func check(r *http.Request) bool {
	return true
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     check,
}

type WsServer struct {
	listener  net.Listener
	Addr      string
	upgrader  websocket.Upgrader
	wsHandler func(*websocket.Conn)
}

func NewWsServer(addr string, hd func(*websocket.Conn)) *WsServer {
	return &WsServer{
		Addr:      addr,
		upgrader:  upgrader,
		wsHandler: hd,
	}
}

func (ws *WsServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := ws.upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Upgrade error:", err)
		return
	}
	fmt.Println("Websocket connected:", fmt.Sprint(conn.RemoteAddr()))
	go ws.wsHandler(conn)
}

func (w *WsServer) Start() (err error) {
	w.listener, err = net.Listen("tcp", w.Addr)
	if err != nil {
		fmt.Println("Net listen error:", err)
		return
	}
	err = http.Serve(w.listener, w)
	if err != nil {
		fmt.Println("HTTP serve error:", err)
		return
	}
	return nil
}
