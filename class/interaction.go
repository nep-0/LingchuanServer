package class

import (
	"fmt"

	"github.com/gorilla/websocket"
)

func (c *class) Boardcast(msg string) {
	for _, v := range c.Students {
		v.send <- msg
	}
}

type interaction struct {
	form    string // disc, abcd, fill, race
	collect map[*websocket.Conn]string
}

var discuss = interaction{
	form:    "disc",
	collect: make(map[*websocket.Conn]string),
}

func (c *class) EndInteraction() {
	c.Boardcast("/disc")
	toSend := "Interaction results:\n"
	fmt.Println("\nInteraction results:")
	for k, v := range c.inter.collect {
		stu, ok := c.Students[k]
		if ok {
			fmt.Println(stu.name, "says:", v)
			toSend += stu.name + " says: " + v + "\n"
		}
	}
	if c.teacher.connected {
		c.teacher.send <- toSend
	}
	c.inter = &discuss
}

func (c *class) Abcd(q string) {
	c.inter = &interaction{
		form:    "abcd",
		collect: make(map[*websocket.Conn]string, 100),
	}
	c.Boardcast("/abcd" + q)
}

func (c *class) Fill(q string) {
	c.inter = &interaction{
		form:    "fill",
		collect: make(map[*websocket.Conn]string, 100),
	}
	c.Boardcast("/fill" + q)
}

func (c *class) Race() {
	c.inter = &interaction{
		form:    "race",
		collect: make(map[*websocket.Conn]string, 1),
	}
	c.Boardcast("/race")
}
