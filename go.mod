module LingchuanServer

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/micmonay/keybd_event v1.1.1
)
