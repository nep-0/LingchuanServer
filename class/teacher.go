package class

import (
	"LingchuanServer/server"
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/micmonay/keybd_event"
)

var twg sync.WaitGroup

var teacherLock sync.Mutex

var kb keybd_event.KeyBonding
var err error

type teacher struct {
	server    *server.WsServer
	send      chan string
	receive   chan string
	auth      func(string, string) bool
	connected bool
}

func (t *teacher) teacherHandle(conn *websocket.Conn) {

	grabbed := false

	go func() {
		time.Sleep(time.Second)
		if !grabbed {
			conn.Close()
			fmt.Println("Don't connect with a second teacher!")
			return
		}
	}()

	teacherLock.Lock()
	defer teacherLock.Unlock()

	grabbed = true

	var name string
	var password string

	conn.SetReadDeadline(time.Now().Add(time.Millisecond * time.Duration(10000)))
	_, msg, err := conn.ReadMessage()
	if err != nil {
		conn.Close()
		fmt.Println("Connection with Teacher closed.")
		return
	}
	name = string(msg)

	// time out
	if name == "" || name == "/htbt" {
		conn.Close()
		fmt.Println("Connection with Teacher closed.")
		return
	}

	conn.SetReadDeadline(time.Now().Add(time.Millisecond * time.Duration(10000)))
	_, msg, err = conn.ReadMessage()
	if err != nil {
		conn.Close()
		fmt.Println("Connection with Teacher closed.")
		return
	}
	password = string(msg)

	// time out
	if password == "" || password == "/htbt" {
		conn.Close()
		fmt.Println("Connection with Teacher closed.")
		return
	}

	if !t.auth(name, password) {
		conn.Close()
		fmt.Println("Connection with Teacher closed.")
		return
	}

	t.send <- "Your name is " + name
	fmt.Println("Teacher is", name)
	t.connected = true

	twg.Add(2)
	go t.receiveRoutine(conn)
	go t.sendRoutine(conn)
	twg.Wait()

}

func (t *teacher) sendRoutine(conn *websocket.Conn) {
	var msg string
	for {
		msg = <-t.send
		if msg == "/exit" {
			twg.Done()
			return
		}
		conn.WriteMessage(1, []byte(msg))
	}

}

func (t *teacher) receiveRoutine(conn *websocket.Conn) {
	defer func() {
		conn.Close()
		t.connected = false
		fmt.Println("Connection with Teacher closed.")
		t.send <- "/exit"
		twg.Done()
	}()
	var msg []byte
	var err error
	for {
		conn.SetReadDeadline(time.Now().Add(time.Second * time.Duration(120)))
		_, msg, err = conn.ReadMessage()
		if err == nil {
			if msg != nil {
				if string(msg) == "/htbt" {
					// Do not process.
				} else {
					t.receive <- string(msg)
				}
				continue
			}
		}
		break
	}
}

func (c *class) teacherMessage() {

	kb, err = keybd_event.NewKeyBonding()
	if err != nil {
		fmt.Println("Permission Error:\nCan't operate keyboard.")
	}

	if runtime.GOOS == "linux" {
		fmt.Println("Pleade wait 2s.")
		time.Sleep(2 * time.Second)
		fmt.Println("Done.")
	}

	var msg string
	for {
		msg = <-c.teacher.receive
		if msg == "/exit" {
			runningWaitGroup.Done()
			return
		}

		if len(msg) >= 5 {
			switch msg[:5] {
			case "/keyp":
				press(msg[5:])
			case "/abcd":
				c.Abcd(msg[5:])
			case "/fill":
				c.Fill(msg[5:])
			case "/race":
				c.Race()
			case "/endi":
				c.EndInteraction()
			default:
				c.Boardcast("Teacher : " + msg)
				fmt.Println("Teacher :", msg)
				c.teacher.send <- "Teacher : " + msg
			}
		} else {
			c.Boardcast("Teacher : " + msg)
			fmt.Println("Teacher :", msg)
			c.teacher.send <- "Teacher : " + msg
		}
	}
}
